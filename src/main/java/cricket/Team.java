package cricket;

import lombok.Data;

@Data
public class Team {

	private final String name;

	private Team(String name) {
		this.name = name;
	}

	public static Team of(String name) {
		return new Team(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
