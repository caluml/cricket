package cricket;

import lombok.Data;

@Data
public class Ball {

	private final Over over;

	private final int number;
	private final Player bowler;
	private final Player batsman;


	@Override
	public String toString() {
		return "the " + Ordinal.of(number) + " ball from " + bowler + " to " + batsman + " in " + over;
	}
}
