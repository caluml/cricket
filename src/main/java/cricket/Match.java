package cricket;

import lombok.Data;

@Data
public class Match {

	private final Series series;

	private final Ground ground;


	@Override
	public String toString() {
		return "the " + ground + " ground in " + series;
	}
}
