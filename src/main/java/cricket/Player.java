package cricket;

import lombok.Data;

@Data
public class Player {

	private final String name;

	private Player(String name) {
		this.name = name;
	}

	public static Player of(String name) {
		return new Player(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
