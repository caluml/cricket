package cricket;

import lombok.Data;

@Data
public class Over {

	private final Session session;

	private final int over;


	@Override
	public String toString() {
		return "the " + Ordinal.of(over) + " over in " + session;
	}
}
