package cricket;

import java.time.Year;

public class Main {

	public static void main(String... args) {
		Team england = Team.of("England");
		Team australia = Team.of("Australia");
		Ground oldTrafford = Ground.of("Old Trafford");
		Player archer = Player.of("Archer");
		Player warner = Player.of("Warner");

		Series series = new Series(england, australia, Format.ODI, Year.of(2020));
		Match match = new Match(series, oldTrafford);
		Innings innings = new Innings(match, australia, 1);
		Session session = new Session(innings, 3, "afternoon");
		Over over = new Over(session, 23);
		Ball ball = new Ball(over, 3, archer, warner);

		Result result = new Result(ball, 4, 0, 0, Delivery.WIDE, Dismissal.RUN_OUT);

		System.out.println(result);
	}
}
