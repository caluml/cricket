package cricket;

import lombok.Data;

@Data
public class Session {

	private final Innings innings;
	private final int day;
	private final String session;

	public Session(Innings innings,
								 int day,
								 String session) {
		this.innings = innings;
		this.day = day;
		this.session = session;
	}

	@Override
	public String toString() {
		return "the " + Ordinal.of(day) + " day " + session + " session in " + innings;
	}
}
