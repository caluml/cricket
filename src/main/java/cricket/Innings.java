package cricket;

import lombok.Data;

@Data
public class Innings {

	private final Match match;
	private final Team team;
	private final int innings;

	@Override
	public String toString() {
		return "the " + team + " " + Ordinal.of(innings) + " innings in " + match;
	}

}
