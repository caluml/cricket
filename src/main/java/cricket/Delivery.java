package cricket;

public enum Delivery {

	FAIR,
	WIDE,
	NO_BALL
}
