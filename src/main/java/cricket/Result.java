package cricket;

import lombok.Data;

@Data
public class Result {

	private final Ball ball;

	private final int runs;
	private final int byes;
	private final int extras;
	private final Delivery delivery;
	private final Dismissal dismissal;

	@Override
	public String toString() {
		return "a " + delivery + " delivery, scoring " + runs + " runs, " + byes + " byes resulting in " + dismissal + " of " + ball;
	}
}
