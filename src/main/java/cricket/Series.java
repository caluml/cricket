package cricket;

import lombok.Data;

import java.time.Year;

@Data
public class Series {

	private final Team home;
	private final Team away;
	private final Format format;
	private final Year year;


	@Override
	public String toString() {
		return "the " + home + " v " + away + " " + year + " " + format + " series";
	}
}
