package cricket;

import lombok.Data;

@Data
public class Ground {

	private final String name;

	private Ground(String name) {
		this.name = name;
	}

	public static Ground of(String name) {
		return new Ground(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
